const domDivImg = document.querySelector('#divImg'),
    domImg = document.querySelector('#avatar'),
    domMsg = document.querySelector('#msg');

let negative = false;


domDivImg.addEventListener('mouseover', clickMessage );

domDivImg.addEventListener('mouseout', noClickMessage );

domDivImg.addEventListener('click', negativeImg );


function clickMessage() {
    domMsg.classList.remove('none');
}

function noClickMessage() {
    domMsg.classList.add('none');
}

function negativeImg() {
    if( !negative ){
        domImg.setAttribute('src', 'img/white-img.jpg');
        
        setTimeout(function(){
            domImg.setAttribute('src', 'img/avatar-992-negatif.jpg')
        }, 750);
        
        negative = true;
        
    }else{
        
        domImg.setAttribute('src', 'img/avatar-992.png');
        
        negative = false;
    }
}
